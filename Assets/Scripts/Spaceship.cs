﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spaceship : MonoBehaviour {

    //levers
    [SerializeField] float rcsThrust;
    [SerializeField] float mainThrust;
    [SerializeField] bool cheatOn = false;
    [SerializeField] AudioClip thrustSound;
    [SerializeField] AudioClip explosionSound;
    [SerializeField] AudioClip success;
    [SerializeField] ParticleSystem engineFire;
    [SerializeField] ParticleSystem explosion;
    [SerializeField] ParticleSystem onSuccess;
    [SerializeField] float levelLoadDelay = 1f;
    [SerializeField] float deathLoadDelay = 2.5f;

    //cached references
    Rigidbody myRigidbody;
    AudioSource audioSource;

    //other properties
    enum State { Alive, Dead, Transcending }
    State state = State.Alive;

	// Use this for initialization
	void Start () {
        myRigidbody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        ProcessInput();
	}

    private void ProcessInput()
    {
        if (state == State.Alive)
        {
            Thrust();
            Rotate();
        }
    }

    private void Rotate()
    {
        //manually control the rotation when contrlling the ship
        myRigidbody.freezeRotation = true;

        float rotationSpeed;
        if (Input.GetKey(KeyCode.A))
        {
            rotationSpeed = Time.deltaTime * rcsThrust;
            transform.Rotate(Vector3.forward * rotationSpeed);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            rotationSpeed = Time.deltaTime * rcsThrust;
            transform.Rotate(Vector3.back * rotationSpeed);
        }

        //restore physics after control done to the ship
        myRigidbody.freezeRotation = false;
    }

    private void Thrust()
    {
        //press space to boost the spaceship
        if (Input.GetKey(KeyCode.Space))
        {
            myRigidbody.AddRelativeForce(Vector3.up * mainThrust * Time.deltaTime * 60);
            if (!audioSource.isPlaying)
            {
                audioSource.PlayOneShot(thrustSound);
            }
            engineFire.Play();
        }
        else
        {
            //as soon as the button is released, stop sound effect

            audioSource.Stop();
            engineFire.Stop();

        }
    }

    void OnCollisionEnter(Collision collision)
    {
        //if the space ship is not on alive state, disable effects on collision
        if(state != State.Alive)
        {
            return;
        }
        switch (collision.gameObject.tag)
        {
            case "Friendly":
                Debug.Log("Safe");
                break;
            case "Finish":
                state = State.Transcending;
                Destroy(collision.gameObject);
                audioSource.Stop();
                onSuccess.Play();
                audioSource.PlayOneShot(success);
                Invoke("LoadNextScene", levelLoadDelay);
                break;
            default:
                Debug.Log("Die");
                if (!cheatOn)
                {
                    audioSource.Stop();
                    explosion.Play();
                    audioSource.PlayOneShot(explosionSound);
                    state = State.Dead;
                    Invoke("ReloadScene", deathLoadDelay);
                }
                break;
        }
    }

    private void ReloadScene()
    {
        Debug.Log(audioSource.isPlaying);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void LoadNextScene()
    {
        SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex + 1) % SceneManager.sceneCountInBuildSettings);
    }
}
