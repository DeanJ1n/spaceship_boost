﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//to prevent more than one of this particular component to exist on a single object
[DisallowMultipleComponent]

public class Oscillator : MonoBehaviour {

    //design levers
    [SerializeField] Vector3 movementVector;
    [Range(0, 1)] [SerializeField] float movementFactor;
    [SerializeField] float period = 2f;

    //data references
    Vector3 startPos;

    // Use this for initialization
    void Start () {
        startPos = transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
        AutoMovement();
    }

    private void AutoMovement()
    {
        //set movementFactor automatically
        if (period <= Mathf.Epsilon)
        {
            return;
        }
        float cycles = Time.time / period;
        const float tau = Mathf.PI * 2;
        float rawSinWave = Mathf.Sin(tau * cycles);
        movementFactor = rawSinWave / 2 + 0.5f;

        transform.position = movementVector * movementFactor + startPos;
    }
}
